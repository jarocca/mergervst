#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 10:57:01 2018

@author: javier
"""

import pandas as pd
import matplotlib.pyplot as plt

def merge_RvsT(T_filename, R_filename, T_sheet='Hoja4', T_skiprows=0,  
           R_header=3, R_skiprows=0, T_timeshift=0, Pt100_current=0.3e-3, interpmet='cubic'):
    df_T = pd.read_excel(T_filename, sheet_name=T_sheet, usecols='A:B', 
                         skiprows=range(1, T_skiprows), 
                         parse_dates=['Time'], convert_float=False)
    df_T['Timediff'] = df_T['Time'] - df_T.Time[0]
    df_T['t'] = df_T['Timediff'].dt.seconds + T_timeshift
    df_T['R_Pt100'] = df_T['VDC'] / Pt100_current
    df_T['T'] = -247.29 + df_T['R_Pt100'] *(2.3992 + df_T['R_Pt100'] * 
        (0.00063962 + 0.0000010241 * df_T['R_Pt100'])) # Qubic fit Pt-100
    df_T.set_index(['t'], inplace=True)

    df_R = pd.read_csv(R_filename, sep='\t', decimal=',', header=R_header - 1,
                       skip_blank_lines=False,
                       skiprows=range(R_header, R_skiprows))
    df_R['DifTiempo'] = df_R['Tiempo (s)'] - df_R['Tiempo (s)'][0]
    df_R.set_index(['DifTiempo'], inplace=True)

    df_RT = df_T.join(df_R, how='outer')
    df_RvsT = df_RT[['T', 'Resist. (Ohm)']].copy()
    df_RvsT['T'].interpolate(method=interpmet, inplace=True)
    df_RvsT = df_RvsT[~df_RvsT['Resist. (Ohm)'].isnull()]

    plt.figure()
    plt.scatter(df_RvsT['T'], df_RvsT['Resist. (Ohm)'])
    Tmax = df_RvsT['T'].idxmax()

    df_heating = df_RvsT[:Tmax]
    plt.figure()
    plt.scatter(df_heating['T'], df_heating['Resist. (Ohm)'])

    df_cooling = df_RvsT[Tmax:]
    plt.figure()
    plt.scatter(df_cooling['T'], df_cooling['Resist. (Ohm)'])

    df_Tvst = df_RT[~df_RT['T'].isnull()].copy()
    df_Tvst = df_Tvst[['T']]
    plt.figure()
    plt.scatter(x=df_Tvst.index, y=df_Tvst.T)

    
    filename = R_filename.split('.')[0]
    df_heating.to_csv(filename + ' - heating.csv', encoding='utf-8')
    df_cooling.to_csv(filename + ' - cooling.csv', encoding='utf-8')
    df_Tvst.to_csv(filename + ' - Tvst.csv', encoding='utf-8')

if __name__ == "__main__":
    import sys
    merge_RvsT(sys.argv[1], sys.argv[2], T_sheet=sys.argv[3], 
           T_skiprows=int(sys.argv[4]), R_header=int(sys.argv[5]), 
           R_skiprows=int(sys.argv[6]), T_timeshift=float(sys.argv[7]), 
           Pt100_current=float(sys.argv[8]), interpmet=sys.argv[9])
